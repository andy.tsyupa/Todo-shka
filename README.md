## Todo-shka (Eleks.Internship)


Student Info

# AndrewTsyupa, KEP ПІ-14-01


Estimate - 40h

Створити програму для обліку поточних завдання (список завдань). 
Користувач повинен мати змогу:


* Додавати нові завдання (після додавання завдання воно зявляється в списку завдань).
* Позначати завдання як виконане (після виконання завдання зникає зі списку).
* Видалити завдання (після видалення завдання зникає зі списку).
* Переглядати історію останніх 10-ти виконаних та видалених завдань.
* Програма повинна зберігати свій стан, тобто після перезапуску списки завдань і історія повинні відновитись.
* Сховище даних обирається самостійно (Database, XML, JSON).



 Previous Internship: https://gitlab.com/AndrewTsyupa/Todo-shka.git.